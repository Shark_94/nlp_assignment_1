The solutions provided in the 2 programs are implemented very much on PURE Python and only nltk is used as a package. Therefore, unlike Method 2, this contains a classification based purly on contextualized sentence probability calculation considering FreqDist only. 
 
method_1_144188B.py
-------------------
The program contains the classification of the 2 sentences only considering the sentence probability using a bigram model. 

method_1_with_machine_learning.py
---------------------------------
This program is an extended version of the above program where an  addtional machcine learning approach considering several classification algorithems : naive bayes, multinominal naive bayes, bernulli naive bayes, support vector classification and linear suport vector classification is also considered. 

This is for the purpose of comparing accuracies of the 2 techniques - machine learning and sentence probability methods.  