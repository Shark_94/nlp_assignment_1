import nltk
from nltk.tokenize import word_tokenize
from nltk.stem import WordNetLemmatizer
from nltk.tokenize import RegexpTokenizer
from nltk.corpus import stopwords
import decimal

ham_messages = []
spam_messages = []

lemmatizer = WordNetLemmatizer()

#begin read_dataset()
def read_dataset():

    #Read the data set
    
    messages = open("../SMSSpamCollection.tsv","r").read()

    try:
        for m in messages.split('\n'):
            split_m = m.split('\t', 1)

            if(split_m[0]==' ' ):
                break
            else:
                annotation = split_m[0]
                content = split_m[1].lower()
                
                if annotation == 'ham':
                    ham_messages.append("<s> "+content+"</s>")

                elif annotation == 'spam':
                    spam_messages.append("<s> "+content+"</s>")

    except Exception as e:
                print(e)

#end read_dataset()

ham_bigram_list = []
spam_bigram_list = []

ham_unigrams = []
spam_unigrams = []

ham_trigram_list =[]
spam_trigram_list = []

tokenizer = RegexpTokenizer("[\w']+")

read_dataset()
   
for m in ham_messages:
    lem_ham_unigrams = map(lemmatizer.lemmatize,tokenizer.tokenize(m))
    ham_unigrams.append(lem_ham_unigrams)
    ham_bigram_list.extend(list(nltk.bigrams(lem_ham_unigrams)))
    ham_trigram_list.extend(list(nltk.trigrams(lem_ham_unigrams)))

for m in spam_messages:
    lem_spam_unigrams = map(lemmatizer.lemmatize,tokenizer.tokenize(m))
    spam_unigrams.append(lem_spam_unigrams)
    spam_bigram_list.extend(list(nltk.bigrams(lem_spam_unigrams)))
    spam_trigram_list.extend(list(nltk.trigrams(lem_spam_unigrams)))

sec_ham_unigrams = []
sec_spam_unigrams = []
for m in ham_messages:
    words = tokenizer.tokenize(m)
    for w in words:
        sec_ham_unigrams.append(lemmatizer.lemmatize(w))
for m in spam_messages:
    words = tokenizer.tokenize(m)
    for w in words:
        sec_spam_unigrams.append(lemmatizer.lemmatize(w))

#frequencies of both ham and spam unigrams
ham_uni_freq = len(set(ham_unigrams))
spam_uni_freq = len(set(spam_unigrams))

ham_count = nltk.FreqDist(list(ham_unigrams)).N()
spam_count = nltk.FreqDist(list(spam_unigrams)).N()

#frequencies of both ham and spam bigrams
ham_freq = nltk.FreqDist(ham_bigram_list)
spam_freq = nltk.FreqDist(spam_bigram_list)

#frequencies of both ham and spam trigrams
tri_ham_freq = nltk.FreqDist(ham_trigram_list)
tri_spam_freq = nltk.FreqDist(spam_trigram_list)

#removes bigrams that are made up only of stopwords
bigram_freq_list = list(ham_freq)+list(spam_freq)
stopwords = set(stopwords.words('english'))
for bg in bigram_freq_list:
    if(bg[0] in stopwords and bg[1] in stopwords):
        del ham_freq[bg]
        del spam_freq[bg]
    elif('lt' in bg or 'gt' in bg or 'le' in bg or 'ge' in bg):
        del ham_freq[bg]
        del spam_freq[bg]
    elif(bg[0].isdigit() and bg[1].isdigit()):
        del ham_freq[bg]
        del spam_freq[bg]

#print(ham_freq.most_common(20))

#begin probability_calculation
def probability_calculation(text):
    lem_text = map(lemmatizer.lemmatize,tokenizer.tokenize(text.lower()))
    sent_bigrams = list(nltk.bigrams(lem_text))
    prob_ham = 1
    prob_spam = 1

    #for ham
    print("For Ham:")
    for bigram in sent_bigrams:
        prob_ham_down = 0
        prob_ham_up = ham_freq[bigram]+1
        print(bigram," : ",prob_ham_up)
        for (fb,sb) in ham_bigram_list:
            if(fb == bigram[0]):
                prob_ham_down += ham_freq[(fb,sb)]

        print("Term frequency : ",prob_ham_down)
        prob = prob_ham_up / (prob_ham_down+ham_uni_freq)
        prob_ham *= prob
    
    print("Vocabulary(Ham) = ",ham_uni_freq)
    
    
    print("\nFor Spam:\n")
    #for spam
    for bigram in sent_bigrams:
        prob_spam_down = 0
        prob_spam_up = spam_freq[bigram]+1
        print(bigram," : ",prob_spam_up)
        for (fb,sb) in spam_bigram_list:
            if(fb == bigram[0]):
                prob_spam_down += spam_freq[(fb,sb)]
                
        print("term frequency : ",prob_spam_down)
        prob = prob_spam_up / (prob_spam_down+spam_uni_freq)
        prob_spam *= prob
        
    print("Vocabulary(Spam) = ",spam_uni_freq)
    

    return (prob_ham,prob_spam)
        
#end probability_calculation

#begin unigram probability_calculation
def unigram_probability_calculation(text):
    lem_text = map(lemmatizer.lemmatize,tokenizer.tokenize(text.lower()))
    sent_unigrams = list(lem_text)
    prob_ham = 1
    prob_spam = 1

    #for ham
    for unigram in sent_unigrams:
        prob_ham_down = ham_count
        prob_ham_up = nltk.FreqDist(list(sec_ham_unigrams)).freq(unigram)
        prob = prob_ham_up / (prob_ham_down)                
        prob_ham *= prob
      
    #for spam
    for unigram in sent_unigrams:
        prob_spam_down = spam_count
        prob_spam_up = nltk.FreqDist(list(sec_spam_unigrams)).freq(unigram)
        prob = prob_spam_up / (prob_spam_down)
        prob_spam *= prob
    
    return (prob_ham,prob_spam)
        
#end unigram probability_calculation

#begin trigram probability_calculation
def trigram_probability_calculation(text):
    lem_text = map(lemmatizer.lemmatize,tokenizer.tokenize(text.lower()))
    sent_trigrams = list(nltk.trigrams(lem_text))
    prob_ham = 1
    prob_spam = 1

    ham_vocab = len(set(ham_bigram_list))
    spam_vocab = len(set(spam_bigram_list))
    
    #for ham
    print("For Ham:")
    for fb,sb,tb in sent_trigrams:
        prob_ham_down = 0
        prob_ham_up = tri_ham_freq[(fb,sb,tb)]+1
        print((fb,sb,tb)," : ",prob_ham_up)
        #Here the bigram requency is directly used
        prob_ham_down = ham_freq[(fb,sb)]

        print("Term frequency : ",prob_ham_down)
        prob = prob_ham_up / (prob_ham_down+ham_vocab)
        prob_ham *= prob
    
    print("Vocabulary(Ham) = ",ham_vocab)
    
    
    print("\nFor Spam:\n")
    #for spam
    for fb,sb,tb in sent_trigrams:
        prob_spam_down = 0
        prob_spam_up = tri_spam_freq[(fb,sb,tb)]+1
        print((fb,sb,tb)," : ",prob_spam_up)
        prob_spam_down = spam_freq[(fb,sb)]

        print("Term frequency : ",prob_spam_down)
        prob = prob_spam_up / (prob_spam_down+spam_vocab)
        prob_spam *= prob
    
    print("Vocabulary(Spam) = ",spam_vocab)
    
    return (prob_ham,prob_spam)
        
#end trigram probability_calculation

test_1 = "Sorry, ..use your brain dear"
test_2 = "SIX chances to win CASH."

#Sentence probability after smoothing
print("THIS IS THE PROGRAM SUBMITTED FOR ASSIGNMENT 1")
print("Smoothing used : Laplace Smoothing\n")

print("\nSentence Probabilities (Unigram):")
t1_prob = unigram_probability_calculation(test_1)
print(test_1,"-\n\tHam:",t1_prob[0],"\n\tSpam: ",t1_prob[1])
if(t1_prob[0]>t1_prob[1]):
    print("\tSentence could be classified : Ham\n")
else:
    print("\tSentence could be classified : Spam\n")

t2_prob = unigram_probability_calculation(test_2)
print(test_2,"-\n\tHam:",t2_prob[0],"\n\tSpam: ",t2_prob[1])
if(t2_prob[0]>t2_prob[1]):
    print("\tSentence could be classified : Ham\n")
else:
    print("\tSentence could be classified : Spam\n")

print("--------------------------------------------------------------------------------")
print("\nSentence Probabilities (Bigram):")
t1_prob = probability_calculation(test_1)
print(test_1,"-\n\tHam:",t1_prob[0],"\n\tSpam: ",t1_prob[1])
if(t1_prob[0]>t1_prob[1]):
    print("\tSentence could be classified : Ham\n")
else:
    print("\tSentence could be classified : Spam\n")

t2_prob = probability_calculation(test_2)
print(test_2,"-\n\tHam:",t2_prob[0],"\n\tSpam: ",t2_prob[1])
if(t2_prob[0]>t2_prob[1]):
    print("\tSentence could be classified : Ham\n")
else:
    print("\tSentence could be classified : Spam\n")

print("\nConsidering above Results, it could be seen Unigrams perform better in terms of accuracy than Bigrams !!")

print("---------------------------------------------------------------------------------")
print("\nSentence Probabilities (Trigram):")
t3_prob = trigram_probability_calculation(test_1)
print(test_1,"-\n\tHam:",t3_prob[0],"\n\tSpam: ",t3_prob[1])
if(t3_prob[0]>t3_prob[1]):
    print("\tSentence could be classified : Ham\n")
else:
    print("\tSentence could be classified : Spam\n")

t3_prob = trigram_probability_calculation(test_2)
print(test_2,"-\n\tHam:",t3_prob[0],"\n\tSpam: ",t3_prob[1])
if(t3_prob[0]>t3_prob[1]):
    print("\tSentence could be classified : Ham\n")
else:
    print("\tSentence could be classified : Spam\n")

print("\nConsidering above Results, it could be seen Unigrams perform is still better in terms of accuracy than Trigrams !!")
