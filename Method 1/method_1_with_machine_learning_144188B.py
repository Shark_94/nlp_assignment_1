import nltk
from nltk.tokenize import word_tokenize
from nltk.stem import WordNetLemmatizer
from nltk.tokenize import RegexpTokenizer
from nltk.classify.scikitlearn import SklearnClassifier
from sklearn.naive_bayes import MultinomialNB,BernoulliNB
from sklearn.linear_model import LogisticRegression, SGDClassifier
from sklearn.svm import SVC, LinearSVC, NuSVC
from sklearn import tree
from nltk.corpus import stopwords
import random
#from nltk.tokenize import TweetTokenizer

#list for each type
ham_messages = []
spam_messages = []

lemmatizer = WordNetLemmatizer()

#begin read_dataset()
def read_dataset():

    #Read the data set and classifies them into ham and spam according to
    #annotation so that two sperate files contains only messages of a
    #particular type.
    
    messages = open("SMSSpamCollection.tsv","r").read()
    
    try:
        for m in messages.split('\n'):
            split_m = m.split('\t', 1)

            if(split_m[0]==' ' ):
                break
            else:
                annotation = split_m[0]
                content = split_m[1].lower()
                
                if annotation == 'ham':
                    ham_messages.append(content)
                elif annotation == 'spam':
                    spam_messages.append(content)

    except Exception as e:
                print(e)
#end read_dataset()

ham_bigram_list = []
spam_bigram_list = []

ham_unigrams = []
spam_unigrams = []

tokenizer = RegexpTokenizer("[\w']+")
#tokenizer = TweetTokenizer()

read_dataset()
    
for m in ham_messages:
    lem_ham_unigrams = map(lemmatizer.lemmatize,tokenizer.tokenize(m))
    ham_unigrams.append(lem_ham_unigrams)
    ham_bigram_list.extend(list(nltk.bigrams(lem_ham_unigrams)))

for m in spam_messages:
    lem_spam_unigrams = map(lemmatizer.lemmatize,tokenizer.tokenize(m))
    spam_unigrams.append(lem_spam_unigrams)
    spam_bigram_list.extend(list(nltk.bigrams(lem_spam_unigrams)))

bigram_list = ham_bigram_list + spam_bigram_list

#frequencies of both ham and spam unigrams
ham_uni_freq = len(set(ham_unigrams))
spam_uni_freq = len(set(spam_unigrams))

#frequencies of both ham and spam bigrams
ham_freq = nltk.FreqDist(ham_bigram_list)
spam_freq = nltk.FreqDist(spam_bigram_list)

#total freqnecy of all bigrams
bigram_freq = nltk.FreqDist(bigram_list)
print(bigram_freq.most_common(20))

#removes bigrams that are made up only of stopwords
bigram_freq_list = list(bigram_freq)
stopwords = set(stopwords.words('english'))
for bg in bigram_freq_list:
    if(bg[0] in stopwords and bg[1] in stopwords):
        del bigram_freq[bg]
        del ham_freq[bg]
        del spam_freq[bg]
    elif('lt' in bg or 'gt' in bg or 'le' in bg or 'ge' in bg):
        del bigram_freq[bg]
        del ham_freq[bg]
        del spam_freq[bg]
    elif(bg[0].isdigit() and bg[1].isdigit()):
        del bigram_freq[bg]
        del ham_freq[bg]
        del spam_freq[bg]

##print(ham_freq)
##print(spam_freq)

##print(ham_freq.most_common(10))
        
#considering the top 1000 bigrams by frequency as the features
bigram_features = list(bigram_freq.keys())[:1000]

#begin find_features
    
def find_features(text):

    #reads through the ham and spam messages individually.
    #Reads through the selected features set and checks whether that feature exisists within that messages.
    #If so, each featue will be marked True, Flase otherwiese.
    #So, the featues and it's label would be mainainted in a dictionary - features
    #The functions returns this dictionary of featue-lable pairs, as a representation of that message.

    lem_text = map(lemmatizer.lemmatize,tokenizer.tokenize(text))
    bigrams = list(nltk.bigrams(lem_text))
    features = {}
    for bg in bigram_features:
        features[bg] = (bg in bigrams)

    return features
#end find_features

features = []
for msg in ham_messages:
    features.append((find_features(msg),"ham"))
    
for msg in spam_messages:
    features.append((find_features(msg),"spam"))

random.shuffle(features)

#selects 3300 messages from teh 5500 messgaes for training.
#rest of the messges for validating the model
training_set = features[:3300]
sample_set = features[3300:]
#print(training_set)

#begin train_models
def train_models():

    #trains several models using different algorithems
    #Checks which algorithem has the best accuracy and resturns that model
    
    Models = []
    
    print("Model\t\t\taccuracy")
    print("Naive Bayes Algorithems")
    #learning a model using several algorithems
    #1 Naive Bayes
    Naive_Bayes_classifier = nltk.NaiveBayesClassifier.train(training_set)
    bn_accuracy = nltk.classify.accuracy(Naive_Bayes_classifier, sample_set)*100
    Models.append((Naive_Bayes_classifier,bn_accuracy))
    print("Naive Bayes\t\t",bn_accuracy)

    #2 Multinominal Naive Bayes
    MNB_classifier = SklearnClassifier(MultinomialNB())
    MNB_classifier.train(training_set)
    mnb_accuracy = nltk.classify.accuracy(MNB_classifier, sample_set)*100
    Models.append((MNB_classifier,mnb_accuracy))
    print("Multinominal\t\t", mnb_accuracy)

    #3 Bernulli Naive Bayes 
    BernoulliNB_classifier = SklearnClassifier(BernoulliNB())
    BernoulliNB_classifier.train(training_set)
    b_accuracy = nltk.classify.accuracy(BernoulliNB_classifier, sample_set)*100
    Models.append((BernoulliNB_classifier,b_accuracy))
    print("Bernoulli\t\t", b_accuracy)

    #4 Logistic Regression
    LogisticRegression_classifier = SklearnClassifier(LogisticRegression())
    LogisticRegression_classifier.train(training_set)
    lr_accuracy = nltk.classify.accuracy(LogisticRegression_classifier, sample_set)*100
    Models.append((LogisticRegression_classifier,lr_accuracy))
    print("Logistic Regression\t", lr_accuracy)

    print("\nSupport Vector Algorithems")
    #5 Support Vector Classification
    SVC_classifier = SklearnClassifier(SVC())
    SVC_classifier.train(training_set)
    svc_accuracy = nltk.classify.accuracy(SVC_classifier, sample_set)*100
    Models.append((SVC_classifier,svc_accuracy))
    print("SVC\t\t\t", svc_accuracy)

    #6 Linear SVC
    LinearSVC_classifier = SklearnClassifier(LinearSVC())
    LinearSVC_classifier.train(training_set)
    lsvc_accuracy = nltk.classify.accuracy(LinearSVC_classifier, sample_set)*100
    Models.append((LinearSVC_classifier,svc_accuracy))
    print("Linear SVC\t\t", lsvc_accuracy)

    selected_model = select_model(Models)
    return selected_model[0]

#end train_models

def select_model(models):
    
    selected = models[5]
    
    for (model,acc) in models:
        if (selected[1] < acc):
            selected = (model,acc)

    return selected 
    
model = train_models()

test_1 = "Sorry, ..use your brain dear"
test_2 = "SIX chances to win CASH."

def check_sentiment(message):
    features = find_features(message.lower())
    return (model.classify(features))

print("According to Classification:")
print(test_1," : ",check_sentiment(test_1))
print(test_2," : ",check_sentiment(test_2))

#begin probability_calculation
def probability_calculation(text):
    lem_text = map(lemmatizer.lemmatize,tokenizer.tokenize(text.lower()))
    sent_bigrams = list(nltk.bigrams(lem_text))
    prob_ham = 1
    prob_spam = 1

    #for ham
    for bigram in sent_bigrams:
        prob_ham_down = 0
        prob_ham_up = ham_freq[bigram]+1
        for (fb,sb) in ham_bigram_list:
            if(fb == bigram[0]):
                prob_ham_down += ham_freq[(fb,sb)]

        prob = prob_ham_up / (prob_ham_down+ham_uni_freq)
        prob_ham *= prob
    
        
    #for spam
    for bigram in sent_bigrams:
        prob_spam_down = 0
        prob_spam_up = spam_freq[bigram]+1
        for (fb,sb) in spam_bigram_list:
            if(fb == bigram[0]):
                prob_spam_down += spam_freq[(fb,sb)]

        prob = prob_spam_up / (prob_spam_down+spam_uni_freq)
        prob_spam *= prob

    return (prob_ham,prob_spam)
        
#end probability_calculation

#Sentence probability after smoothing   
print("\nSentence Probabilities:")
t1_prob = probability_calculation(test_1)
print(test_1,"- Ham:",t1_prob[0]," Spam: ",t1_prob[1])
if(t1_prob[0]>t1_prob[1]):
    print("Sentence could be classified : Ham\n")
else:
    print("Sentence could be classified : Spam\n")

t2_prob = probability_calculation(test_2)
print(test_2,"- Ham:",t2_prob[0]," Spam: ",t2_prob[1])
if(t2_prob[0]>t2_prob[1]):
    print("Sentence could be classified : Ham\n")
else:
    print("Sentence could be classified : Spam\n")

