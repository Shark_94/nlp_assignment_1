import nltk
from nltk.tokenize import word_tokenize
from nltk.stem import WordNetLemmatizer
from nltk.tokenize import RegexpTokenizer
from nltk.corpus import stopwords
import pandas as pd
import re
import numpy as np
from sklearn.feature_extraction.text import CountVectorizer,TfidfTransformer
from sklearn.naive_bayes import MultinomialNB
from sklearn.metrics import classification_report
import string

test_data = ['Sorry, ..use your brain dear','SIX chances to win CASH']

#Step 1
#Preprocessing the data set
fullCorpus = pd.read_csv('../SMSSpamCollection.tsv',sep='\t',header=None,names=['label','body_text'])

#removes punctuations from messages
def remove_punctuations(messages):
    text = [word for word in messages if word not in string.punctuation]
    text = ''.join(text)
    return text

#tokenizing
def tokenize(text):
    tokens = re.split('\W+',text)
    return tokens

#stopword removal
stopwords = nltk.corpus.stopwords.words('english')

def remove_stopwords(tokenized_list):
    text = [word for word in tokenized_list if word not in stopwords]
    return text

#lemmatizing
wn = nltk.WordNetLemmatizer()

def clean_text(tokeized_text):
    text = [wn.lemmatize(word) for word in tokeized_text]
    return text

def join_text(sentence):
    return ' '.join(sentence)

def text_preprocess():
    fullCorpus['body_text'] = fullCorpus['body_text'].apply(lambda x: remove_punctuations(x))
    fullCorpus['body_text_tokeized'] = fullCorpus['body_text'].apply(lambda x: tokenize(x.lower()))
    fullCorpus['body_text_nostop'] = fullCorpus['body_text_tokeized'].apply(lambda x: remove_stopwords(x)) 
    fullCorpus['body_text_lemmatized'] = fullCorpus['body_text_nostop'].apply(lambda x: clean_text(x))
    fullCorpus['body_text_lemmatized'] = fullCorpus['body_text_lemmatized'].apply(lambda x: join_text(x))

text_preprocess()

#End of Preprocessing

training_set = fullCorpus['body_text_lemmatized'][:3220]
test_set = fullCorpus['body_text_lemmatized'][3220:]
training_labels = fullCorpus['label'][:3220]
test_labels = fullCorpus['label'][3220:]

#Step 2
#Extracting n-grams
#1 Unigrams
unigram_vec = CountVectorizer(stop_words="english",analyzer="word",ngram_range=(1,1), max_df=1.0,min_df=1,max_features=None)
count_unigram = unigram_vec.fit(training_set)
unigrams = unigram_vec.transform(training_set)

#2 Bigrams
bigram_vec = CountVectorizer(stop_words="english",analyzer="word",ngram_range=(2,2), max_df=1.0,min_df=1,max_features=None)
count_unigram = bigram_vec.fit(training_set)
bigrams = bigram_vec.transform(training_set)

#3 Trigrams
trigram_vec = CountVectorizer(stop_words="english",analyzer="word",ngram_range=(3,3), max_df=1.0,min_df=1,max_features=None)
count_trigram = trigram_vec.fit(training_set)
trigrams = trigram_vec.transform(training_set)

#4 Combination of all 3
combo_vec = CountVectorizer(stop_words="english",analyzer="word",ngram_range=(1,3), max_df=1.0,min_df=1,max_features=None)
count_combo = combo_vec.fit(training_set)
combos = combo_vec.transform(training_set)
#End of Extracting n-grams

#Step 3
#Frequency Distributions
#1 Unigrams
uni_tfid = TfidfTransformer().fit(unigrams)
tfidfs_unigrams = uni_tfid.transform(unigrams)

#2 Bigrams
bi_tfid = TfidfTransformer().fit(bigrams)
tfidfs_bigrams = bi_tfid.transform(bigrams)

#3 Trigrams
tri_tfid = TfidfTransformer().fit(trigrams)
tfidfs_trigrams = tri_tfid.transform(trigrams)

#3 Trigrams
combo_tfid = TfidfTransformer().fit(combos)
tfidfs_combos = combo_tfid.transform(combos)

#End of Frequency Distribution

#Step 4
#Calculating Probability
#Model defenition
unigram_model = MultinomialNB().fit(tfidfs_unigrams,training_labels)
bigram_model = MultinomialNB().fit(tfidfs_bigrams,training_labels)
trigram_model = MultinomialNB().fit(tfidfs_trigrams,training_labels)
combo_model = MultinomialNB().fit(tfidfs_combos,training_labels)

#1 Unigram
uni_test_CountVec = unigram_vec.transform(test_data)
uni_test_tfids = uni_tfid.transform(uni_test_CountVec)
unimodel_test_CountVec = unigram_vec.transform(test_set)
unimodel_test_tfids = uni_tfid.transform(unimodel_test_CountVec)


#2 Bigram
bi_test_CountVec = bigram_vec.transform(test_data)
bi_test_tfids = bi_tfid.transform(bi_test_CountVec)
bimodel_test_CountVec = bigram_vec.transform(test_set)
bimodel_test_tfids = bi_tfid.transform(bimodel_test_CountVec)

#3 Trigram
tri_test_CountVec = trigram_vec.transform(test_data)
tri_test_tfids = tri_tfid.transform(tri_test_CountVec)
trimodel_test_CountVec = trigram_vec.transform(test_set)
trimodel_test_tfids = tri_tfid.transform(trimodel_test_CountVec)

#3 Combo
combo_test_CountVec = combo_vec.transform(test_data)
combo_test_tfids = combo_tfid.transform(combo_test_CountVec)
combomodel_test_CountVec = combo_vec.transform(test_set)
combomodel_test_tfids = combo_tfid.transform(combomodel_test_CountVec)

#End of Calculating Probability


#Step 5
#Evaluation
print("\nUnigram Model")
unigram_result = unigram_model.predict(uni_test_tfids)
print(unigram_result)
unigrammodel_result = unigram_model.predict(unimodel_test_tfids)
print (classification_report(test_labels, unigrammodel_result))

print("\nBigram Model")
bigram_result = bigram_model.predict(bi_test_tfids)
print(bigram_result)
bigrammodel_result = bigram_model.predict(bimodel_test_tfids)
print (classification_report(test_labels, bigrammodel_result))

print("\nTrigram Model")
trigram_result = trigram_model.predict(tri_test_tfids)
print(bigram_result)
trigrammodel_result = trigram_model.predict(trimodel_test_tfids)
print (classification_report(test_labels, trigrammodel_result))

print("\nCombined Model")
combo_result = combo_model.predict(combo_test_tfids)
print(bigram_result)
combomodel_result = combo_model.predict(combomodel_test_tfids)
print (classification_report(test_labels, combomodel_result))

#End of Evaluation


