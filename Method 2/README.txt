method_2_144188B.py
-------------------

This program is implemented based on the process indicated in the lab session. Therefore, the preprocessing steps are from the lab session. 

The program uses CounterVectorizer and TFIDFVectorizer to count the frequencies of the tokens which will be used to train the Maltinominal Naive Bayes models. 

Here 4 models are built, considering Unigrams, Bigrams, Trigrams and a combination of Uni/Bi\Trigrams. The preformance of each of teh models are also evaluted.   